# Node Author System
Node.js + Express4 + Passport でログイン認証の仕組みを作る。

## Usage
### 1.起動
```
npm start
```
### 2.アクセス
[localhost:3000/home/index](localhost:3000/home/index)

## 経緯
JavaScriptでログイン認証したいと思い調べながら実装してみました。
[Node.js + Express + passport で 認証認可 の 仕組み を 作る - galife](https://garafu.blogspot.com/2017/02/express-passport-authn-authz.html) を見ながら認証システムを作成していましたが、自分の環境では動かなかったので試行錯誤しながらなんとかログインできるようになりました。

## 環境
- Ubuntu 16.04
- Node.js 10.6.0
- npm 6.1.0
- express 4.16.0
- bower 1.8.4
- MongoDB 4.0.0

## 環境構築

```
$ sudo apt update
$ sudo apt install nodejs         # Node.jsのインストール
$ sudo apt install npm            # npmのインストール
$ sudo npm install bower          # bowerのインストール
$ sudo npm install express --save # expressのインストール
```

## Welcome to Express
雛形作成のコマンド
オプションについては  ->  [Express 4.x系のインストールとコマンド](https://qiita.com/yoh-nak/items/868c55e04ce2dd128ccc)
```terminal
$ express -e NodeAutherSystem
```
プロジェクトの実行コマンド
```terminal
$ npm start
```
この後にブラウザの`localhost:3000`にアクセスして結果を確認する。  
`Welcome to Express`と表示されていたら成功。  
```terminal
Error: Cannot find module 'モジュール名'
```
というエラーが発生した時は
```
$ sudo npm install --save 表示されたモジュール名
```
を実行することで解決できる。
また足りないモジュールを一気にインストールしてくれる
```terminal

$ npm-install-missing
```
という便利なコマンドもある。

## 機能表
このプロジェクトで実現する機能一覧です.

| 機能名 | メゾッド | URL | 認可要否 |
|:------|:------|:----|:--------|
|トップ画面を表示する  |GET|/home/index|不要|
|ログイン画面を表示する|GET|/account/login|不要|
|ログインする        |POST|/account/login|不要|
|ログアウトする       |POST|/account/logout|必要|
|プロフィール画面を表紙する|GET|/account/profile|必要|

## 画面遷移図
![画面遷移図](https://4.bp.blogspot.com/-p0WkbyYr-JY/WJc0C2ET5pI/AAAAAAAADv8/U4EjZk18ceomZuZ85qcJCOiieS2sgBmYACPcB/s1600/auth-01.png)
> 引用: [Node.js + Express + passport で 認証認可 の 仕組み を 作る](https://garafu.blogspot.com/2017/02/express-passport-authn-authz.html)

## Package
プロジェクトで使用するパッケージの一覧及びインストール方法です。
[npm](https://gitlab.com/ogahiro21/NodeAuterSystem#npm), [bower](https://gitlab.com/ogahiro21/NodeAuterSystem#bower), [MongoDB](https://gitlab.com/ogahiro21/NodeAuterSystem#mongodb)の３つに分けて解説しています。
### npm

- **Summary**

|パッケージ|概要|依存|
|:------|:--|:--|
|mongoose|MongoDBにアクセスライブラリ|-|
|body-parser|POSTパラメーターをJSONで取得|-|
|cookie-parser|cookieを解析するミドルウェア|-|
|express-session|セッション管理を行うモジュール|-|
|connect-flash|フラッシュメッセージの表示|cookie-parser, express-session|
|passport|Node.jsのための認証ミドルウェア|body-parser, connect-flash|
|passport-local|usernameとpasswordで認証|passport|

- **Install**

 ```terminal
$ sudo npm install --save mongoose
$ sudo npm install --save body-parser
$ sudo npm install --save cookie-parser
$ sudo npm install --save express-session
$ sudo npm install --save connect-flash
$ sudo npm install --save passport
$ sudo npm install --save passport-local
```
`--save`コマンドを使うと`package.json`の`dependencies`に追記されます.

- **npm init**  
npm initについては[こちら](https://gitlab.com/ogahiro21/TechNote/blob/master/Node.md#npm-init)を参照。

### bower
- **Summary**

|パッケージ|概要|依存|
|:------|:--|:--|
|jQuery|JavaScriptの有名ライブラリ|-|
|font-awesome|フォントが増える|-|
|bootstrap|デザインがいい感じになる|jQuery|

- **Initalize**  
bowerの保存先を`/public/third_party`としたいので`.bowerrc`ファイルを作成する。

```/.bowerrc
{
  "directory": "public/third_party"
}
```

- **Install**

 ```terminal
$ bower install jquery --save
$ bower install font-awesome --save
$ bower install bootstrap --save
```
`bower install jquery --save` は `bower i jquery -S`と省略して書ける.

- **bower init**  
bower initについては[こちら](https://gitlab.com/ogahiro21/TechNote/blob/master/Node.md#bower)

### MongoDB

- **Install**

 ```terminal
$ sudo apt-get install mongodb
```
- **How to use**

 mongoシェルの起動

 ```terminal
$ mongo
MongoDB shell version: 4.0.0
connecting to: test
Welcome to the MongoDB shell.
For interactive help, type "help".
For more comprehensive documentation, see
	http://docs.mongodb.org/
Questions? Try the support group
	http://groups.google.com/group/mongodb-user
  >
```

 DBを作成

 ```terminal
> use DB名
　
> db.createCollection("カラム")
　
> db.カラム.insert({
... id: "yamada",
... email: "yamada@sampele.com",
... name: "yamada",
... password: password,
... role: "group1"
...})
```

## Explain codes
中で何が動いているかの説明をします。
### /app.js
- L 16

```app.js
mongoose.connect("mongodb://localhost:27017/sample", { useNewUrlParser: true });
```

> We can connect to MongoDB with the `mongoose.connect()` method.   
> This is the minimum needed to connect the `sample` database.  

と[公式ホームページ](http://mongoosejs.com/docs/connections.html)にありましたので`mongoose.connect("mongodb://localhost:27017/sample");`とだけ書いて実行したところ、`{ useNewUrlParser: true }` を書いてくださいというエラーが発生したので追加したところ問題なく動きました。なぜそうなるかは調べがつきませんでした...
`27017`という数字はmongodbのデフォルトポート番号です。

- L 18 ~ 31  

```app.js
// passportがユーザー情報をシリアライズすると呼び出される
passport.serializeUser(function(id,done){
     done(null,id);
});
// passportがユーザー情報をデシリアライズすると呼び出される
passport.deserializeUser(function(id,done){
  User.findById(id, (error,user) => {
    if(error){
      return done(error);
    }
    done(null,user);
  });
});
```
`done()` : 要するに，doneはEventEmitter的に，「成功したよ」「失敗したよ」とPassport内部に対して通知するイベントを発行しているようなもの．

シリアライズ・デシリアライズについては[Express+Passportで簡単に認証機能を実現 - Qiita](https://qiita.com/papi_tokei/items/9b852774114ebc7a6255#32-%E3%82%B7%E3%83%AA%E3%82%A2%E3%83%A9%E3%82%A4%E3%82%BA%E3%83%87%E3%82%B7%E3%83%AA%E3%82%A2%E3%83%A9%E3%82%A4%E3%82%BA)の記事が個人的に分かりやすかったです。
この記事によるとシリアライズとは
>簡単に言うと、データをファイルとして保存できる形式に変換することを、シリアライズといいます。
通常、オブジェクトデータはオブジェクトグラフと呼ばれるデータ構造になっており、一つを保存すれば良い、とはならないようです。
リンク状になったデータを保存できるように直列に繋ぐ作業を、シリアライズという（らしい）。

- L 33 ~ 54

```app.js
passport.use(
    "local-login",
    new LocalStrategy({
        usernameField: "username",
        passwordField: "password",
        passReqToCallback: true
    }, function (request, username, password, done) {
        process.nextTick(() => {
            User.findOne({ "email": username }, function (error, user) {
                if (error) {
                    return done(error);
                }
                if (!user || user.password != password) {
                    return done(null, false, request.flash("message", "Invalid username or password."));
                }
                // 保存するデータは必要最低限にする
                return done(null, user._id);
            });
        });
    })
);
```

- `passport.use()`
第1引数には後からストラテジーを呼び出す名前を指定する(L101で呼び出している).第２引数として`LocalStrategy`を指定.

- `new LocalStrategy()`
第１引数にはログイン画面に指定している、「ユーザー名」と「パスワード」の名前を入れる。
例えば`usernameField`にはユーザー名を示す**テキストボックス**の名前が指定される。
第2引数では認証処理を定義している。

- L 40 ~ 53 **認証処理**　　
`process.nextTick()`はイベントループの順番を変える的な働き。ここでは次のイベントループにユーザー認証を割り込ませている。  
L46 `done()`で渡した値は`passport.serializeUser()`のコールバックの第一引数になる。
>  参考: [Understanding process.nextTick()を翻訳してみた。](https://yosuke-furukawa.hatenablog.com/entry/20120511/1336755959)  

- L56 ~ 65

```app.js
var authorize = function (role) {
    return function (request, response, next) {
        if (request.isAuthenticated() &&
            request.user.role === role) {
            return next();
        }
        response.redirect("/account/login");
    };
};
```
引数の`role`について: 権限をまとめて定義して、それに名前をつけたもの。
rollもroleも使い方的には一緒。
> 参考: [ロール (roll)とは｜「分かりそう」で「分からない」でも「分かった」気になれるIT用語辞典](https://wa3.i-3-i.info/word11273.html)



## Reference
[Node.js + Express + passportで認証認可の仕組みを作る](https://garafu.blogspot.com/2017/02/express-passport-authn-authz.html)

[npmの使い方 - Qiita](https://qiita.com/msakamoto_sf/items/a1ae46979a42d6948ebd)

[Mongoose v5.1.7: Connecting to MongoDB](http://mongoosejs.com/docs/connections.html)

[Understanding passport serialize desirialize - Stack Overflow](https://stackoverflow.com/questions/27637609/understanding-passport-serialize-deserialize)
