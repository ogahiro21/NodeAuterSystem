var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var bodyParser = require("body-parser");
var flash = require("connect-flash");
var session = require("express-session");
var mongoose = require("mongoose");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var User = require("./models/user.js");

// MongoDB 接続先設定
mongoose.connect("mongodb://localhost:27017/sample", { useNewUrlParser: true });

// passport が ユーザー情報をシリアライズすると呼び出されます
passport.serializeUser(function (id, done) {
    done(null, id);
});

// passport が ユーザー情報をデシリアライズすると呼び出されます
passport.deserializeUser(function (id, done) {
    User.findById(id, (error, user) => {
        if (error) {
            return done(error);
        }
        done(null, user);
    });
});

// passport における具体的な認証処理を設定します。
passport.use(
    "local-login",
    new LocalStrategy({
        usernameField: "username",
        passwordField: "password",
        passReqToCallback: true
    }, function (request, username, password, done) {
        process.nextTick(() => {
            User.findOne({ "email": username }, function (error, user) {
                if (error) {
                    return done(error);
                }
                if (!user || user.password != password) {
                    return done(null, false, request.flash("message", "Invalid username or password."));
                }
                // 保存するデータは必要最低限にする
                return done(null, user._id);
            });
        });
    })
);

// 認可処理。指定されたロールを持っているかどうか判定します。
var authorize = function (role) {
    return function (request, response, next) {
        if (request.isAuthenticated() &&
            request.user.role === role) {
            return next();
        }
        response.redirect("/account/login");
    };
};

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// ミドルウェアの設定
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(flash());
app.use("/public", express.static("public"));

// 雛形
app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

// passport設定
app.use(session({ secret: "some salt", resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

// ルーティング設定
app.use("/", (function () {
    var router = express.Router();
    router.get("/home/index", function (request, response) {
        response.render("./home/index.ejs");
    });
    router.get("/account/login", function (request, response){
        response.render("./account/login.ejs", { message: request.flash("message") });
    });
    router.post("/account/login", passport.authenticate(
            "local-login", {
                successRedirect: "/account/profile",
                failureRedirect: "/account/login"
            }));
    router.post("/account/logout", authorize("group1"), function (request, response){
        request.logout();
        response.redirect("/home/index");
    });
    router.get("/account/profile", authorize("group1"), function (req, res, next){
      res.render('account/profile', {user : req.user});
    });
    return router;
})());


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
